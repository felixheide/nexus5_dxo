# you can use 'make' or 'mall all' to run all the images
# 
# or you can use e.g. 'make 01' to run image script N5_good_0001.json


PYTHON = python

PIPELINE_PATH = /home/felix/projects/ALGOLUX/research/core/isp/pipeline.py

PIPELINE_CMD = ${PYTHON} ${PIPELINE_PATH}

all: 095

% : DSC_0%.json generic_all_param.json
	${PIPELINE_CMD} execute --image_param $< --isp_param ./generic_all_param.json --vl_off
	
generate:
	${PIPELINE_CMD} generate

clean:
	rm -rf output

